function main() {
    function init() {
        const submit = document.getElementById('submit-button');
        submit.addEventListener('click', sendData);
        $(document).ready(function () {
            $("#locales").change(function () {
                const selectedOption = $('#locales').val();
                if (selectedOption !== '') {
                    window.location.replace('card?lang=' + selectedOption);
                }
            });
        });
    }

    function sendData() {
        const form = document.getElementById("form");
        const formEntries = new FormData(form).entries();
        const json = Object.assign(...Array.from(formEntries, ([x, y]) => ({[x]: y})));
        fetch("/record", {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(json),
        }).then((response) => {
            form.reset();
            alert('send');

        });
    }
    init();
}
main();