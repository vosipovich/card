package com.task.card.controller;

import com.task.card.entity.Record;
import com.task.card.repository.RecordRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RecordController {
    private final RecordRepository recordRepository;

    public RecordController(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    @PostMapping("/record")
    public Record greetingSubmit(@RequestBody Record record) {
        return recordRepository.save(record);
    }

    @GetMapping("/allrecords")
    public Page<Record> getAllRecords(@RequestParam(required = false, defaultValue = "0") Integer page) {
        return recordRepository.findAll(PageRequest.of(page, 10));
    }

    @GetMapping("/recordsbyname")
    public List<Record> getRecordsByName(@RequestParam(required = false, defaultValue = "") String name) {
        return recordRepository.findByName(name);
    }
}
